function select(e){
   // const form = document.getElementById("myForm"); //niet nodig hier
    const selection = document.getElementById("selection");
    //of als alternatief om de select tag vast te krijgen via de name:
    //const selection = document.getElementsByName("choice")[0];
    const option = selection.options[selection.selectedIndex];
    let bod = document.getElementsByTagName("body")[0];
    bod.style.backgroundColor=option.value;

}

function init(){
    const selection=document.getElementById("selection");
    selection.addEventListener("change",select);
}
window.addEventListener("load",init);