function setCookie(name,content,days=0,hours=0,minutes=0){
    const now = new Date();
    const exp = new Date(now.getTime()+(((((days*24)+hours)*60)+minutes)*60000));
    const expiration=exp.toUTCString();
    name = encodeURIComponent(name);
    content = encodeURIComponent(content);
    document.cookie = `${name}=${content};expires=${expiration}`;
}

function getCookie(name){
    const regex = new RegExp(name + "=([^;]+)");
    const result = regex.exec(document.cookie);
    if(result)
        return decodeURIComponent(result[1]);
    else
        return null;
}

function clearCookie(name){
    setCookie(name,"",-1);
}

const textTypes = ["text","color","date","datetime","datetime-local","email",
"month","number","range","search","tel","time","url","week"];

function loadForm(form){
    for(let i=0; i<form.elements.length;i++){
        if(textTypes.indexOf(form.elements[i].type)>=0){
            form.elements[i].value=getCookie(form.elements[i].name)||"";
        }
    }
}

function saveForm(form){
    for(let i=0; i<form.elements.length;i++){
        if(textTypes.indexOf(form.elements[i].type)>=0){
            setCookie(form.elements[i].name,form.elements[i].value,0,1,0);
        }
    }
}
