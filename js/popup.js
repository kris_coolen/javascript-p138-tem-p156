function init(){
    let area = document.getElementById("area");
    area.addEventListener("mousedown",mouseDown);
}

function mouseDown(e){

    const popup = document.getElementById("popup");
    popup.style.visibility="visible";
    popup.style.left=(e.clientX-15)+"px";
    popup.style.top=(e.clientY-15)+"px";
}

window.addEventListener("load",init);


