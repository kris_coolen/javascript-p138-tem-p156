function toEnglish(){
    setCookie("lang","english",0,0,5);
    window.location.replace("english.html");
}

function toFrench(){
    setCookie("lang","francais",0,0,5);
    window.location.replace("french.html");
}

function toDutch(){
    setCookie("lang","nederlands",0,0,5);
    window.location.replace("dutch.html");
}

function init(){
   var lang =getCookie("lang");
   //alert(lang);
   if(lang==null){
       return;
   }
   else{
       switch(lang){
           case "english":
                window.location.replace("english.html");
                break;
            case "francais":
                window.location.replace("french.html");
                break;
            case "nederlands":
                window.location.replace("dutch.html");
       }
   }
}
window.addEventListener("load",init);