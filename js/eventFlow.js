

function documentBubble(e){
    console.log("Bubble Document");
}

function documentCapture(e){
    console.log("Capture Document");
}

function area1MouseBubble(e){
    console.log("Bubble Area1");
}

function area1MouseCapture(e){
    console.log("Capture Area1");
}
function area2MouseBubble(e){
    console.log("Bubble Area2");
}

function area2MouseCapture(e){
    console.log("Capture Area2");
    e.stopPropagation();
}

function init(){
    const area1 = document.getElementById("area1");
    const area2 = document.getElementById("area2");
    document.addEventListener("click",documentCapture,true);
    document.addEventListener("click",documentBubble,false);
    area1.addEventListener("click",area1MouseCapture,true);
    area1.addEventListener("click",area1MouseBubble,false);
    area2.addEventListener("click",area2MouseCapture,true);
    area2.addEventListener("click",area2MouseBubble,false);
}

window.addEventListener("load",init);