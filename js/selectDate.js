
const months=["January","February","March","April","May","June","July","August","September","Oktober","November","December"];
const thirtyDays = [1,3,5,8,10];
var daySelection, monthSelection,yearSelection;

function leapYear(year)
{
  return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}

function buildOptions(){

    for(let i=1; i<=31; i++){
        var option = document.createElement("option");
        option.text=i;
        daySelection.add(option);
    }
    

    
    for(const month of months){
        var option = document.createElement("option");
        option.text=month;
        monthSelection.add(option);
    }
    
    for(let i=2000; i<=2020; i++){
        var option = document.createElement("option");
        option.text=i;
        yearSelection.add(option);
    }
}

function init(){
    daySelection=document.getElementById("daySelection");
    monthSelection=document.getElementById("monthSelection");
    yearSelection=document.getElementById("yearSelection");
        
    buildOptions();
    
    daySelection.addEventListener("change",select);
    monthSelection.addEventListener("change",select);
    yearSelection.addEventListener("change",select);


}   
function select(e){
    
    const dayOption = daySelection.options[daySelection.selectedIndex];

    const monthOption=monthSelection.options[monthSelection.selectedIndex];
   
    const yearOption=yearSelection.options[yearSelection.selectedIndex];

   //reset all days to enabled
   for(let i=0; i<daySelection.options.length;i++){
       daySelection.options[i].disabled=false;
   }
    //reset all months to enabled
    for(let i=0; i<monthSelection.options.length;i++){
        monthSelection.options[i].disabled=false;
    }

    //reset all years to enabled
    for(let i=0; i<yearSelection.options.length;i++){
        yearSelection.options[i].disabled=false;
    }
    

   //day is selected
    if(dayOption.text=="31"){//day 31 selected
        for(let mIndex of thirtyDays){
            monthSelection.options[mIndex].disabled=true;//maanden van 30 dagen
        }
        monthSelection.options[1].disabled=true; //maand februari
    }
    else if(dayOption.text=="30"){//day 30 is selected
        monthSelection.options[1].disabled=true;//maand februari
    }

    //month is selected
    if(monthOption.text==months[1]){
        daySelection.options[29].disabled=true;
        daySelection.options[30].disabled=true;
    }
    for(let mIndex of thirtyDays){
        if(monthOption.text==months[mIndex]){
            daySelection.options[30].disabled=true;
        }
    }
    //29 februari is selected ->disable all non leap years

    if(dayOption.text=="29"&&monthOption.text=="February"){
      for(let i=0; i<yearSelection.options.length;i++){
            if(!leapYear(yearSelection.options[i].text)){
                yearSelection.options[i].disabled=true;
            }
        }
    }

    //if a non leaf year is selected and month febraury also we must disable day 29
    if(!leapYear(yearOption.text)&&monthOption.text=="February"){
        daySelection.options[28].disabled=true;
    }



    //if a non leaf year is selected and we choose 29 then februari must be disabled
    if(!leapYear(yearOption.text)&&dayOption.text=="29"){
        monthSelection.options[1].disabled=true;//maand februari disabelen
    }
    
}
window.addEventListener("load",init);