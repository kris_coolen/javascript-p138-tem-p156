function validate(e){
    const form = document.getElementById("myForm");
    let validFirstName=true;
    let validLastName=true;
    let validMail =true;
    let validBirthDay=true;
    const firstNameField = document.getElementById("firstname");
    const lastNameField = document.getElementById("lastname");
    const mailField=document.getElementById("mail");
    const bdayField=document.getElementById("bday");
    let mailregex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})$/;
    let bdayregex = /^(0?[1-9]|[12][0-9]|3[01])[\/\-\.](0?[1-9]|1[012])[\/\-\.]\d{4}$/;

    if(firstNameField.value.length==0){
        validFirstName=false;
        firstNameField.setAttribute("class","error");
    } else{
        firstNameField.removeAttribute("class");
    }

    if(lastNameField.value.length==0){
        validLastName=false;
        lastNameField.setAttribute("class","error");
    } else{
        lastNameField.removeAttribute("class");
    }

    if(!mailregex.test(mailField.value)){
       validMail=false;
       mailField.setAttribute("class","error");
    }
    else{
        mailField.removeAttribute("class");
    }

    if(!bdayregex.test(bdayField.value)){
        validBirthDay=false;
        bdayField.setAttribute("class","error");
    }
    else{
        bdayField.removeAttribute("class");
    }

    if(!validLastName){
        const errorElement=document.getElementById("lastNameError");
        errorElement.innerHTML="Enter your lastname!";
        e.preventDefault();
    }
    if(!validFirstName){
        const errorElement=document.getElementById("firstNameError");
        errorElement.innerHTML="Enter your firstname.";
        e.preventDefault();
    }
    if(!validMail){
       const errorElement=document.getElementById("mailError");
       errorElement.innerHTML="Enter a valid email adress.";
       e.preventDefault();
    }
    if(!validBirthDay){
        const errorElement=document.getElementById("birthdayError");
        errorElement.innerHTML="Enter a valid birthday.";
        e.preventDefault();
    }
    saveForm(form);
}

function reset(e){
    document.getElementById("firstNameError").innerHTML="";
    document.getElementById("lastNameError").innerHTML="";
    document.getElementById("mailError").innerHTML="";
    document.getElementById("birthdayError").innerHTML="";
    document.getElementById("firstname").removeAttribute("class");
    document.getElementById("lastname").removeAttribute("class");
    document.getElementById("mail").removeAttribute("class");
    document.getElementById("bday").removeAttribute("class");
   
}

function init(){
    const form = document.getElementById("myForm");
    form.addEventListener("submit",validate);
    form.addEventListener("reset",reset);
    loadForm(form);

}
window.addEventListener("load",init);