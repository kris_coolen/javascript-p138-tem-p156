function getParameter(name){
    const queryString = decodeURIComponent(location.search.replace(/\+/g,"%20"));
    const regex = new RegExp(name+"=([^&]+)");
    const result = regex.exec(queryString);
    if(result)
        return result[1];
    else
        return null;
}

function init(){
    let gr = document.getElementById("greet");
    const firstName = getParameter("firstName");
    const lastName = getParameter("lastName");
    gr.innerHTML="Hello "+firstName + " "+lastName;
}

window.addEventListener("load",init);